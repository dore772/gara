import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'gara';

  images = [
    {
      'title': 'assets/films/Placeholder ebook (1).png'
    },
    {
      'title': 'assets/films/Placeholder ebook (2).png'
    },
    {
      'title': 'assets/films/Placeholder ebook (3).png'
    },
    {
      'title': 'assets/films/Placeholder ebook (4).png'
    },
    {
      'title': 'assets/films/Placeholder ebook.png'
    },
    {
      'title': 'assets/films/Placeholder ebook (1).png'
    },
    {
      'title': 'assets/films/Rectangle 2025.png'
    },
    {
      'title': 'src/assets/films/Rectangle 2025.png'
    },
    {
      'title': 'src/assets/films/Rectangle 2025.png'
    },
    {
      'title': 'src/assets/films/Rectangle 2025.png'
    }
    ,
    {
      'title': 'src/assets/films/Rectangle 2025.png'
    },
    {
      'title': 'src/assets/films/Rectangle 2025.png'
    }
  ];

}
